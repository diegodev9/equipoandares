Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #get 'admincontact/new'
  #get 'user/new' => 'user#new'
  #
  resources :user, only: [:new, :create]
  resources :contact, only: [:new, :create]
  resources :admincontact, only: [:new, :create]

  get '/services' => 'static_pages#services'
  get '/staff' => 'static_pages#staff'
  get '/news' => 'static_pages#news'
  get '/training' => 'static_pages#training'
  get '/contact' => 'contact#new'
  get '/work_with_us' => 'user#new'
  get '/alert_message' => 'static_pages#alert_message'
  get '/admincontact' => 'admincontact#new'
  get '/faq' => 'static_pages#faq'
  get '/working' => 'static_pages#working'
  get '/history' => 'static_pages#history'
  get '/how' => 'static_pages#how'
  get '/info' => 'static_pages#info'

  post 'user/new' => 'user#create'
  post 'contact/new' => 'contact#create'
  post 'admincontact/new' => 'admincontact#create'

  root 'static_pages#home'
end
