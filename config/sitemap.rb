require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'https://www.equipoandares.com.ar'
SitemapGenerator::Sitemap.create do
  add '/home', :changefreq => 'weekly'
  add '/contact', :changefreq => 'weekly'
end
SitemapGenerator::Sitemap.ping_search_engines