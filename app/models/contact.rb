class Contact < MailForm::Base

  attribute :nombre,    :validate => true
  attribute :apellido,  :validate => true
  attribute :asunto,    :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :mensaje,   :validate => true
  attribute :nickname,  :captcha  => true

  def headers
    {
        :subject => "Contacto desde Web Andares",
        :to => "andaresequipo@gmail.com",


    }
  end

end