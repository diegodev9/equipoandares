class Admincontact < MailForm::Base

  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :mensaje,   :validate => true
  attribute :nickname,  :captcha  => true

  def headers
    {
        :subject => 'Admin Contact desde Web Andares',
        :to => 'diegodev9@gmail.com',


    }
  end

end