class User < MailForm::Base

  attribute :profesion, :validate => true
  attribute :nombre, :validate => true
  attribute :apellido, :validate => true
  attribute :direccion, :validate => true
  attribute :ciudad, :validate => true
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :mensaje
  attribute :file, :attachment => true
  attribute :nickname,  :captcha  => true

  def headers
    {
        :subject => "Curriculum desde Web Andares",
        :to => "andaresequipo@gmail.com",


    }
  end

end