class StaticPagesController < ApplicationController
  def home
    @class_home = 'active'
    @header_image = 'images/img_bg_1.jpg'
  end

  def services
    @class_services = 'active'
    @header_image = 'images/services12.jpg'
  end

  def staff
    @class_who = 'active'
    @header_image = 'images/IMG_20190416_114246.jpg'
  end

  def news
    @class_info = 'active'
    @header_image = 'images/news.jpg'
  end

  def training
    @class_training = 'active'
    @header_image = 'images/training.jpg'
  end

  def faq
    @class_info = 'active'
    @header_image = 'images/dudas.jpg'
  end

  def working
    @header_image = 'images/working.jpg'
  end

  def history
    @class_who = 'active'
    @header_image = 'images/andares_old9.jpg'
  end

  def how
    @class_who = ' active'
    @header_image = 'images/how_portada.jpg'
  end

  def info
    @class_info = 'active'
    @header_image = 'images/info.jpg'
  end

  def alert_message
    @header_image = 'images/gracias.jpg'
  end

end
