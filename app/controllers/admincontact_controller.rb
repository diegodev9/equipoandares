class AdmincontactController < ApplicationController

  def new
    @header_image = 'images/developer_working.jpg'
    @admincontact = Admincontact.new(params[:admincontact])
  end

  def create
    @admincontact = Admincontact.new(params[:admincontact])
    @admincontact.request = request
    if !@admincontact.deliver
      puts('error al enviar')
      redirect_to root_path
    else
      @admincontact = Admincontact.new
      puts('Envio con exito')

      redirect_to root_path
    end
  end

end
