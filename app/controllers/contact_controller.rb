class ContactController < ApplicationController

  def new
    @class_contact = 'active'
    @header_image = 'images/img_bg_1.jpg'
    @contact = Contact.new(params[:contact])
  end

  def create
    @contact = Contact.new(params[:contact])
    @contact.request = request
    if !@contact.deliver
      puts('error al enviar')
      $alert_error = true
      $alert_message = 'Hubo un error al enviar su mensaje, intente nuevamente'
      redirect_to '/alert_message'
    else
      @contact = Contact.new
      puts('Envio con exito')
      $alert_message = 'Su mensaje fue enviado'
      redirect_to '/alert_message'
    end

  end

end
