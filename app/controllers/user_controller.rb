class UserController < ApplicationController

  def new
    @header_image = 'images/grupalandares.jpg'
    @user = User.new(params[:user])
  end

  def create
    @user = User.new(params[:user])
    @user.request = request
    if !@user.deliver
      puts('error al enviar')
      $alert_error = true
      $alert_message = 'Hubo un error al enviar su mensaje, intente de nuevo'
      redirect_to '/alert_message'
    else
      @user = User.new
      puts('Envio con exito')

      $alert_message = 'Su mensaje fue enviado'
      redirect_to '/alert_message'
    end
  end

end
